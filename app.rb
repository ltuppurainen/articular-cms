# ================================
# ================================
#
# ARTICULAR CMS
#
# ================================
# ================================

# ================================
# START BUNDLER
# ================================

require 'rubygems'
require 'bundler/setup'
require 'openssl'
require 'base64'
require 'yaml'

Bundler.require(:default)

# ================================
# END BUNDLER
# ================================

# ================================
# START CONFIGURATION
# ================================

configure do
	@conf = YAML.load_file('./config.yaml')
	$home_page = @conf['home_page']
	$default_group = @conf['default_group']
	$admin_user = @conf['admin_user']
	$site_title = @conf['site_title']
	if @conf["production"]==true
		set :environment, :production
	else
		set :environment, :development
	end
	set :bind, @conf['bind']
	set :port, @conf['port']
	enable :sessions
	set :session_secret, @conf['session_secret']
	MongoMapper.connection = Mongo::Connection.new(@conf['mongo_connection'])
	MongoMapper.database = @conf['mongo_database']
	renderer = Redcarpet::Render::HTML.new(filter_html: true, hard_wrap: true)
	$markdown = Redcarpet::Markdown.new(renderer, extensions = {autolink: true, tables: true})
	$salt = @conf["password_salt"]
	$digest = OpenSSL::Digest::SHA512.new

end

# ================================
# END CONFIGURATION
# ================================

# ================================
# START HELPERS
# ================================

def encrypt_pass(pass)
	return Base64.encode64(OpenSSL::PKCS5.pbkdf2_hmac(pass, $salt, 64000, 128, $digest)).force_encoding("UTF-8")
end

def match_pass(pass, match)
	@passbytes = encrypt_pass(pass).bytes
	@matchbytes = match.bytes
	@matches = 0
	@passbytes.each_with_index do |byte, index|
		if byte == @matchbytes[index]
			@matches += 1
		end
	end
	if @matches == @passbytes.length
		return true
	else
		return false
	end
end
			

# ================================
# END HELPERS
# ================================

# ================================
# START MODELS
# ================================

class User
	
	include MongoMapper::Document
	
	key :name, String, :required => true, :unique => true
	key :pass, String, :required => true
	key :group, Integer
	
end

class Article
	
	include MongoMapper::Document
	
	key :title, String, :required => true, :unique => true
	key :body, String, :required => true
	
	timestamps!
	
end

User.ensure_index(:name)
Article.ensure_index(:title)

# ================================
# END MODELS
# ================================

# ================================
# START CONTROLLERS
# ================================

get '/' do
	redirect to('/a/'+$home_page)
end

get '/login' do
	if !(session[:user]).nil?
		redirect to('/')
	end
	
	if params['a'] == '1'
		@alert = 'Login failed.'
	end
	
	if params['a'] == '2'
		@alert = 'You must sign in to edit articles.'
	end
	
	erb :login, :layout => :layout
end

get '/register' do
	if !(session[:user]).nil?
		redirect to('/')
	end
	
	if params['a'] == '1'
		@alert = 'Registration failed.'
	end
	
	erb :register, :layout => :layout
end

get '/a/:article/edit' do
	if !(session[:user]).nil?
		@user = session[:user]
	end
	@title = params[:article].capitalize
	@article = Article.first(:title => @title)
	if !@article.nil?
		@body = @article.body
	else
		@body = " "
	end
	erb :edit, :layout => :layout
end

get '/a/:article' do
	if !(session[:user]).nil?
		@user = session[:user]
	end
	@title = params[:article].capitalize
	@article = Article.first(:title => @title)
	if !(@article.nil?)
		@body = $markdown.render(@article.body)
		erb :article, :layout => :layout
	else
		redirect to('/a/'+@title+'/edit')
	end
end

get '/logout' do
	session.clear
	redirect to('/')
end


post '/login' do
	@name = params[:name]
	@pass = params[:pass]
	@user = User.first(:name => @name)
	if (!(@user.nil?) and (match_pass(@pass, @user.pass)))
		session[:user] = @user.name
		redirect to('/')
	else
		redirect to('/login?a=1')
	end
end

post '/register' do
	@name = params[:name]
	@pass = params[:pass]
	if (User.first(:name => @name).nil? and @name.to_s.length > 3)
		@user = User.new
		@user.name = @name
		@user.pass = encrypt_pass(@pass)
		if @name == $admin_user
			@user.group = 2
		else
			@user.group = $default_group
		end
		@user.save
		redirect to('/login')
	else
		redirect to('/register?a=1')
	end
end

post '/a/:article/edit' do
	@name = session[:user]
	@user = User.first(:name =>@name)
	if !(@user.nil?)
		@title = params[:article].capitalize
		@body = params[:body]
		@article = Article.first(:title => @title)
		if (@article.nil?)
			@new = Article.new
			@new.title = @title
			@new.body = @body
			@new.save
		else
			@article.body = @body
			@article.save
		end
		redirect to('/a/'+@title)
	else
		redirect to('/login?a=2')
	end
end

post '/jump' do
	if !(params[:search].blank?)
		@search = params[:search].capitalize
		redirect to('/a/'+@search)
	else
		redirect to('/')
	end
end


not_found do
	erb :notfound, :layout => :layout
end

# ================================
# END CONTROLLERS
# ================================